# README

## Dependencias

* curl
* Git
* Ruby version 3.2.1
* Rails version 7.0.6

## Puesta en marcha

* Desde el directorio desde donde quieras crear el proyecto:
  
<pre>
git clone https://gitlab.com/oscarbtbst/rails-course-edition-search.git
cd rails-course-edition-search
bundle install
rails s
</pre>
  

## Prueba de validación

* Desde el directorio del proyecto ejecutar los siguientes comandos:

<pre>
cd test/scripts
./test.sh
</pre>
