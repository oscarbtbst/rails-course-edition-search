class CourseEditionSearchController < ApplicationController
  def search
    begin
      json_body = JSON.parse(request.body.read)
    rescue JSON::ParserError => e
      render json: {error: "Invalid JSON format"}, status: :unprocessable_entity
    else

      #Paso 1: Obtener lista de ediciones de cursos
      course_editions_list =  []
      json_body["editions"].each do |edition|
        edition["courses"].each do |course|
          course_editions_list.push({
            date: edition["date"],
            name: course["name"],
            type: course["type"]
          })
        end
      end

      #Paso 2: Filtrar los datos por tipo o escuela
      json_body["criteria"].each do |criteria_elem|
        criteria_elem_array = criteria_elem.split('-')
        criteria_first = criteria_elem_array[0]
        if ['type', 'school'].include?(criteria_first)
          criteria_type_value = criteria_elem_array.drop(1).join('-')
          course_editions_list = course_editions_list.select do |course_edition| 
            if criteria_first == 'type'
              criteria_type_value == course_edition[:type] 
            else
              criteria_type_value == get_school_from_type(course_edition[:type])
            end
          end
        end
      end

      #Paso 3: Filtrar los datos por fecha
      today = "2023-04-13".to_date #Fecha de hoy ficticia en el que se creó el fichero de test-cases.txt
      filter_date = nil
      if json_body["criteria"].include?("closest") or json_body["criteria"].include?('latest')
        course_editions_future = course_editions_list.select {|course_edition| course_edition[:date].to_date > today}

        if course_editions_future.length > 0
          course_editions_future_sorted = course_editions_future.sort_by {|course_edition| (course_edition[:date].to_date - today)}

          if json_body["criteria"].include?("closest")
            #Si criteria incluye 'closest', obtener fecha más cercana en el futuro
            filter_date = course_editions_future_sorted.first[:date]
          else
            #Si criteria incluye 'latest', obtener fecha más alejada en el futuro
            filter_date = course_editions_future_sorted.last[:date]
          end
          course_editions_list = course_editions_list.filter{|edition| edition[:date] == filter_date}
        end
      end      

      #Paso 4: Formatear la respuesta
      json_response = []
      edition_dates = course_editions_list.map {|course_edition| course_edition[:date]}.uniq
      edition_dates.each do |date|
        course_editions_filtered = course_editions_list.select {|course_edition| course_edition[:date] == date}
        json_response.push({
          date: date,
          courses: course_editions_filtered.map {|course_edition| course_edition[:name]}
        })
      end

      render json: json_response
    end
  end

  private

    def get_school_from_type(type)
      case type
      when "infantil"
        "educacion"
      when "competencias-digitales"
        "educacion"
      when "gobernanza"
        "educacion"
      when "portugues"
        "lenguas"
      when "educacion-artistica"
        "ciencia-y-cultura"
      when "divulgacion-cientifica"
        "ciencia-y-cultura"
      when "cooperacion"
        "cooperacion"
      else
        ""      
      end
    end
end